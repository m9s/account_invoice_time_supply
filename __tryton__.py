# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Account Invoice Time Supply',
    'name_de_DE': 'Fakturierung Lieferzeitraum',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''Account Invoice Time Supply
    - Adds fields for the definition of time supply to the invoice
''',
    'description_de_DE': '''Fakturierung Lieferzeitraum
    - Fügt Felder für die Eingabe eines Lieferzeitraums zu Rechnungen hinzu
''',
    'depends': [
        'account_invoice'
    ],
    'xml': [
        'invoice.xml'
    ],
    'translation': [
         'locale/de_DE.po',
    ],
}
