# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelWorkflow, ModelView, ModelSQL, fields
from trytond.modules.account_invoice.invoice import _STATES, _DEPENDS


class Invoice(ModelWorkflow, ModelSQL, ModelView):
    _name = 'account.invoice'

    time_of_supply_start = fields.Date('Time of Supply Start', states=_STATES,
        depends=_DEPENDS)
    time_of_supply_end = fields.Date('Time of Supply End', states=_STATES,
        depends=_DEPENDS)

    def _credit(self, invoice):
        res = super(Invoice, self)._credit(invoice)
        res['time_of_supply_start'] = invoice.time_of_supply_start
        res['time_of_supply_end'] = invoice.time_of_supply_end
        return res

Invoice()
